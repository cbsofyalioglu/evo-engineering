const path = require("path")
const { i18n } = require("./next-i18next.config")

const ContentSecurityPolicy = `
  default-src 'self';
  script-src 'self';
  child-src evo-e.com;
  style-src 'self' evo-e.com;
  font-src 'self';
`

const securityHeaders = [
    {
      key: 'X-Frame-Options',
      value: 'SAMEORIGIN'
    },
    {
      key: 'X-Content-Type-Options',
      value: 'nosniff'
    },
    //{
    //  key: 'Content-Security-Policy',
    //  value: ContentSecurityPolicy.replace(/\s{2,}/g, ' ').trim()
    //},
    {
      key: 'Referrer-Policy',
      value: 'origin-when-cross-origin'
    }
]


module.exports = {
  // output: "export",
  // distDir: "dist",
    async headers() {
        return [
          {
            // Apply these headers to all routes in your application.
            source: '/:path*',
            headers: securityHeaders,
          },
        ]
      },
    env: {
        host:
            process.env.NODE_ENV === "production"
                ? "https://www.evo-e.com"
                : "http://localhost:3000"
    },
    images: {
        deviceSizes: [360, 480, 576, 768, 1200, 1920, 2048, 3840],
        imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
        minimumCacheTTL: 60,

        domains: ["res.cloudinary.com", "vercel.com", "evo-e.com", "www.evo-e.com"]
    },
    reactStrictMode: true,
    sassOptions: {
        includePaths: [path.join(__dirname, "css")]
    },
    trailingSlash: true,
    devIndicators: {
        buildActivity: false
    },
    eslint: {
        // Warning: This allows production builds to successfully complete even if
        // your project has ESLint errors.
        ignoreDuringBuilds: true
    },
    typescript: {
        // !! WARN !!
        // Dangerously allow production builds to successfully complete even if
        // your project has type errors.
        // !! WARN !!
        ignoreBuildErrors: true
    },
    i18n,
  async redirects() {
    return [
      {
        source: '/careers/open-positions',
        destination: 'https://evo-engineering-gmbh.jobs.personio.de',
        permanent: true,
      },
      {
        source: '/careers/open-positions/',
        destination: 'https://evo-engineering-gmbh.jobs.personio.de',
        permanent: true,
      }
    ]
  },
}
