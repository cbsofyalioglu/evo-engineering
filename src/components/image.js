export default function Image(props) {
    return (
        <img
            style={{
                ...(props.width && { width: props.width }),
                ...(props.height && { height: props.height }),
            }}
            src={props.src}
            alt={props.alt}
            className={props.className}
        />
    )
}