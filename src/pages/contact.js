import Head from "next/head"
import { Fragment, useRef, useEffect, useState } from "react"
import HeaderTwo from "../components/header/header-2"
// import ContactForm from "../components/contact/contact-form"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import nextI18NextConfig from "../../next-i18next.config"
import { useTranslation } from "next-i18next"
import { MailIcon, PhoneIcon, PrinterIcon } from "@heroicons/react/outline"
import { useWindowSize } from "../lib/use-window-size.jsx"
import { styled } from "../styles/stitches.config"
import * as SeparatorPrimitive from "@radix-ui/react-separator"

const StyledSeparator = styled(SeparatorPrimitive.Root, {
    backgroundColor: "#888",
    margin: "64px 0 32px 0",
    height: 2,
    borderRadius: 8
})

const cachedScriptStatuses = {}

// function getScriptNode(src) {
//     const node = document.querySelector(
//         `script[src="${src}"]`,
//     )
//     const status = node?.getAttribute('data-status')

//     return {
//         node,
//         status,
//     }
// }

function ContactPage(props) {
    const { t, i18n } = useTranslation("common")
    const [size, setSize] = useState({ width: 600, height: 400 })
    const myref = useRef(null)


    useEffect(() => {
        if (myref && myref.current) {
            const attrs = myref.current.getBoundingClientRect()
            const w = Math.floor(attrs.width)
            const h = 400

            if (w && h && w !== size.width) {
                setSize({ width: w, height: h })
            }
            console.log("wh", w, h)
        }
    })



    return (
        <Fragment>
            <Head>
                <title>{t("pages.contact.metaTitle")}</title>
                <meta
                    name="description"
                    content={t("pages.contact.metaDescription")}
                />
            </Head>
            <div className="bg-white" id="contact-page">
                <div
                    className="max-w-5xl mx-auto py-24 px-4 sm:py-32 sm:px-6 lg:px-8"
                    ref={myref}
                >
                    <h1 className="text-4xl font-extrabold text-gray-900 lg:text-6l">
                        {t("pages.contact.title")}
                    </h1>

                    <div className="mt-16 grid grid-cols-1 gap-10 sm:grid-cols-2 lg:grid-cols-3">
                        <div>
                            <h3 className="text-lg font-medium text-gray-900 lg:text-xl">
                                {t("pages.contact.office.title")}
                            </h3>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: t("pages.contact.office.content")
                                }}
                                className="text-base text-gray-500 mt-8"
                            />
                        </div>
                        <div>
                            <h3 className="text-lg font-medium text-gray-900 lg:text-xl">
                                {t("pages.contact.communication.title")}
                            </h3>
                            <div className="mt-8 space-y-6">

                                <span className="flex text-base text-gray-500">
                                    <MailIcon
                                        className="flex-shrink-0 w-5 h-5 text-gray-700"
                                        aria-hidden="true"
                                    />
                                    <span
                                        className="ml-3"
                                        dangerouslySetInnerHTML={{
                                            __html: t("pages.contact.email")
                                        }}
                                    />
                                </span>

                                <span className="flex text-base text-gray-500">
                                    <PhoneIcon
                                        className="flex-shrink-0 w-5 h-5 text-gray-700"
                                        aria-hidden="true"
                                    />
                                    <span
                                        className="ml-3"
                                        dangerouslySetInnerHTML={{
                                            __html: t("pages.contact.tel")
                                        }}
                                    />
                                </span>

                                <span className="flex text-base text-gray-500">
                                    <PrinterIcon
                                        className="flex-shrink-0 w-5 h-5 text-gray-700"
                                        aria-hidden="true"
                                    />
                                    <span
                                        className="ml-3"
                                        dangerouslySetInnerHTML={{
                                            __html: t("pages.contact.fax")
                                        }}
                                    />
                                </span>
                            </div>
                        </div>
                    </div>
                    <StyledSeparator css={{ backgroundColor: "#ccc" }} />
                    <div className="flex flex-col items-center relative">
                        <iframe
                            src="https://www.openstreetmap.org/export/embed.html?bbox=11.580845117568972%2C48.186235912391346%2C11.588977575302124%2C48.18993747627774&amp;layer=mapnik&amp;marker=48.188084939558024%2C11.584911346435547"
                            width={size.width}
                            className="max-w-full"
                            height={size.height}
                            allowFullScreen=""
                            loading="lazy"
                            referrerPolicy="no-referrer-when-downgrade"
                        ></iframe>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}


// function useScript(
//     src,
//     options,
// ) {
//     const [status, setStatus] = useState(() => {
//         if (!src || options?.shouldPreventLoad) {
//             return 'idle'
//         }

//         if (typeof window === 'undefined') {
//             // SSR Handling - always return 'loading'
//             return 'loading'
//         }

//         return cachedScriptStatuses[src] ?? 'loading'
//     })

//     useEffect(() => {
//         if (!src || options?.shouldPreventLoad) {
//             return
//         }

//         const cachedScriptStatus = cachedScriptStatuses[src]
//         if (cachedScriptStatus === 'ready' || cachedScriptStatus === 'error') {
//             // If the script is already cached, set its status immediately
//             setStatus(cachedScriptStatus)
//             return
//         }

//         // Fetch existing script element by src
//         // It may have been added by another instance of this hook
//         const script = getScriptNode(src)
//         let scriptNode = script.node

//         if (!scriptNode) {
//             // Create script element and add it to document body
//             scriptNode = document.createElement('script')
//             scriptNode.src = src
//             scriptNode.async = true
//             scriptNode.setAttribute('data-status', 'loading')
//             document.body.appendChild(scriptNode)

//             // Store status in attribute on script
//             // This can be read by other instances of this hook
//             const setAttributeFromEvent = (event) => {
//                 const scriptStatus =
//                     event.type === 'load' ? 'ready' : 'error'

//                 scriptNode?.setAttribute('data-status', scriptStatus)
//             }

//             scriptNode.addEventListener('load', setAttributeFromEvent)
//             scriptNode.addEventListener('error', setAttributeFromEvent)
//         } else {
//             // Grab existing script status from attribute and set to state.
//             setStatus(script.status ?? cachedScriptStatus ?? 'loading')
//         }

//         // Script event handler to update status in state
//         // Note: Even if the script already exists we still need to add
//         // event handlers to update the state for *this* hook instance.
//         const setStateFromEvent = (event) => {
//             const newStatus = event.type === 'load' ? 'ready' : 'error'
//             setStatus(newStatus)
//             cachedScriptStatuses[src] = newStatus
//         }

//         // Add event listeners
//         scriptNode.addEventListener('load', setStateFromEvent)
//         scriptNode.addEventListener('error', setStateFromEvent)

//         // Remove event listeners on cleanup
//         return () => {
//             if (scriptNode) {
//                 scriptNode.removeEventListener('load', setStateFromEvent)
//                 scriptNode.removeEventListener('error', setStateFromEvent)
//             }

//             if (scriptNode && options?.removeOnUnmount) {
//                 scriptNode.remove()
//             }
//         }
//     }, [src, options?.shouldPreventLoad, options?.removeOnUnmount])

//     return status
// }


export async function getStaticProps({ locale }) {
    return {
        props: {
            ...(await serverSideTranslations(
                locale,
                ["common"],
                nextI18NextConfig
            ))
        }
    }
}

export default ContactPage
