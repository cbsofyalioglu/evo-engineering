import React, { useRef, useEffect, useState, Fragment } from "react"
import Head from "next/head"
import Image from "next/image"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import nextI18NextConfig from "../../../next-i18next.config"
import { useTranslation } from "next-i18next"
import { styled } from "../../styles/stitches.config"
import * as SeparatorPrimitive from "@radix-ui/react-separator"
import { FadingHero } from "../../components/fading-hero"
import Link from "next/link"
import { PageLayout } from "../../layout/page-layout"
import { motion, useInView } from "framer-motion"
import { VIDEO_POSTER_FORMAT, VIDEO_SOURCE_FORMATS } from "../../settings/constant"



const BenefitsPerksPage = (props) => {
  const { t, i18n } = useTranslation("common")

  return (
    <Fragment>
      <Head>
        <title>{t("pages.workEnvironment.metaTitle")}</title>
      </Head>
      <div className="bg-white">
        {/* <FadingHero
                    src={"/img/teams-out.webp"}
                    title={t("pages.about.title")}
                    description={t("pages.about.mission.description")}
                /> */}
        <div className="w-full h-auto flex flex-col items-stretch"  id="benefits-page">

          {/* CAREERS SECTIONS  */}
          <PageLayout>
            <div className="w-full relative min-h-[50vh]">
              <img
                src={"/img/female-it.webp"}
                className="rounded-lg z-10 absolute inset-0 w-full h-full object-cover"
                alt="EVO-E Vision"
              />
            </div>
            <div className="flex flex-col items-center">

              {/* TITLE  */}
              <div className="text-center flex flex-col items-center mt-20 my-8">
                <h1 className="text-7xl font-extrabold tracking-tight text-gray-900 sm:text-7xl text-left">
                  {t("pages.benefits.title")}
                </h1>
                <h1 style={{ color: "#FC7F33" }} className="text-7xl font-extrabold tracking-tight sm:text-7xl">{t("pages.workEnvironment.title2")}</h1>
              </div>
              <p className="mt-4 text-lg !sm:text-xl text-warm-gray-500 z-10 my-16">{t("pages.benefits.description")}</p>

              {/* SECTIONS  */}
              <div className="grid items-center grid-cols-1 lg:items-stretch md:grid-cols-2 lg:grid-cols-3 gap-y-8 gap-x-8">
                {
                  [...Array(13).keys()].map((ix) => (
                    <Benefit
                      key={`benefit-${ix}`}
                      title={t(`pages.benefits.item${ix + 1}.title`)}
                      descriptionHtml={t(`pages.benefits.item${ix + 1}.descriptionHtml`)}
                      svg={t(`pages.benefits.item${ix + 1}.icon`)}
                    />
                  ))
                }
              </div>
            </div>


          </PageLayout>
        </div>
      </div>
    </Fragment>
  )
}

const BenefitBox = styled("div", {
  display: "flex",
  flexDirection: "column",
  alignItems: "flex-start",
  gap: "1rem",
  height: "100%",
})

const Benefit = ({title, descriptionHtml, svg}) => (
  <BenefitBox className="benefit">
    <div dangerouslySetInnerHTML={{ __html: svg }} />
    <h4 className="text-[20px] leading-6 min-h-12 capitalize font-[600]">{title}</h4>
    <div dangerouslySetInnerHTML={{ __html: descriptionHtml }} />
  </BenefitBox>
)
export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(
        locale,
        ["common"],
        nextI18NextConfig
      )),
    },
  }
}

export default BenefitsPerksPage
