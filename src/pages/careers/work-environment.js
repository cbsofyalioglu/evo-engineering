import React, { useRef, useEffect, useState, Fragment } from "react"
import Head from "next/head"
import Image from "next/image"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import nextI18NextConfig from "../../../next-i18next.config"
import { useTranslation } from "next-i18next"
import { styled } from "../../styles/stitches.config"
import * as SeparatorPrimitive from "@radix-ui/react-separator"
import { FadingHero } from "../../components/fading-hero"
import Link from "next/link"
import { PageLayout } from "../../layout/page-layout"
import { motion, useInView } from "framer-motion"
import { VIDEO_POSTER_FORMAT, VIDEO_SOURCE_FORMATS } from "../../settings/constant"



const CareerPage = (props) => {
  const { t, i18n } = useTranslation("common")

  return (
    <Fragment>
      <Head>
        <title>{t("pages.workEnvironment.metaTitle")}</title>
      </Head>
      <div className="bg-white">
        {/* <FadingHero
                    src={"/img/teams-out.webp"}
                    title={t("pages.about.title")}
                    description={t("pages.about.mission.description")}
                /> */}
        <div className="w-full h-auto flex flex-col items-stretch">


          {/* CAREERS SECTIONS  */}
          <PageLayout>
            <div className="w-full relative min-h-[50vh]">
              <img
                src={"/img/teams-out.webp"}
                className="rounded-lg z-10 absolute inset-0 w-full h-full object-cover"
                alt="EVO-E Vision"
              />
            </div>
            <div className="flex flex-col items-center">


              {/* TITLE  */}
              <div className="text-center flex flex-col items-center mt-20 my-8">
                <h1 className="text-7xl font-extrabold tracking-tight text-gray-900 sm:text-7xl text-left">
                  {t("pages.workEnvironment.title1")}
                </h1>
                <h1 style={{ color: "#FC7F33" }} className="text-7xl font-extrabold tracking-tight sm:text-7xl">{t("pages.workEnvironment.title2")}</h1>
              </div>
              <div className="mt-4 text-lg !sm:text-xl text-warm-gray-500 z-10 my-16" 
                dangerouslySetInnerHTML={{ __html: t("pages.workEnvironment.description") }}
              />

              {/* SECTIONS  */}
              {[...Array(6).keys()].map((ix) => (
                <Testimonial
                  title={t(`pages.workEnvironment.item${ix + 1}.title`)}
                  subtitle={t(`pages.workEnvironment.item${ix + 1}.subtitle`)}
                  description={t(`pages.workEnvironment.item${ix + 1}.description`)}
                  videoFilenameWithoutExtension={t(`pages.workEnvironment.item${ix + 1}.video`)}
                  key={ix}
                  id={ix}

                />
              ))}
            </div>


          </PageLayout>
        </div>
      </div>
    </Fragment>
  )
}

const Testimonial = ({ title, subtitle, description, videoFilenameWithoutExtension }) => {
  const vref = useRef(null)
  const isInView = useInView(vref)
  // const [showButton, setShowButton] = useState(true)

  const [sizes, setSizes] = useState({
    width: 1920,
    height: 1080
  })
  // const playVideo = () => {
  //   setShowButton(false);
  //   }
  const pauseVideo = () => vref.current.pause()

  useEffect(() => {
    if (vref && vref.current) {
      const currentSize = {
        width: vref.current.clientWidth,
        height: vref.current.clientHeight
      }
      if (
        Math.floor(sizes.width) !==
        Math.floor(vref.current.clientWidth)
      ) {
        setSizes(currentSize)
      }
    }
  }, [vref])

  useEffect(() => {
    if (!isInView && !vref.current.paused) {
      pauseVideo()
    }
  }, [isInView])
  return (
    <section className="py-8 bg-gray-50 sm:py-12 w-full mt-8">
      <div className="max-w-6xl px-4 mx-auto sm:px-6">
        <div className="grid items-center grid-cols-1 lg:items-stretch md:grid-cols-2 gap-y-8 gap-x-12 xl:gap-x-20">
          <div className="relative">
            <div className="aspect-w-4 aspect-h-3">
              <video
                ref={vref}
                className="object-cover w-full h-full"
                width={sizes.width}
                height={sizes.height}
                preload="auto"
                type="video/mp4"
                controls={true}
                poster={`${videoFilenameWithoutExtension}.${VIDEO_POSTER_FORMAT}`}
              >
                {VIDEO_SOURCE_FORMATS.map((format) => (
                  <source
                    key={format}
                    src={`${videoFilenameWithoutExtension}.${format.replace("video/", "")}`} type={format}
                  />
                ))}
              </video>
            </div>
          </div>

          <div className="flex flex-col justify-start md:py-3">
            <h3 className="!tracking-tighter text-3xl font-medium text-gray-900 !font-bold">{title}</h3>
            <h4 className="!tracking-tighter text-xl font-medium text-gray-500 !font-bold my-1">{subtitle}</h4>
            <p className="text-xl leading-relaxed text-gray-700 my-2">{description}</p>

          </div>
        </div>
      </div>
    </section>
  )
}

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(
        locale,
        ["common"],
        nextI18NextConfig
      )),
    },
  }
}

export default CareerPage
