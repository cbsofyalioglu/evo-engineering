import Head from "next/head"
import { Fragment, useRef, useEffect, useState } from "react"
import Image from "next/image"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import nextI18NextConfig from "../../next-i18next.config"
import { useTranslation } from "next-i18next"
import { styled } from "../styles/stitches.config"
import * as SeparatorPrimitive from "@radix-ui/react-separator"
import { useViewportScroll, useTransform, motion } from "framer-motion"

import teamImage from "../../public/img/team-management.webp"
import allenSmead from "../../public/img/team/allen-smead.webp"
import danielSkibiorski from "../../public/img/team/daniel-skibiorski.webp"
import emreBoyacigil from "../../public/img/team/emre-boyacigil.webp"
import evaSchneider from "../../public/img/team/eva-schneider.webp"
import matthiasAxenbeck from "../../public/img/team/matthias-axenbeck.webp"
import romanSennefelder from "../../public/img/team/roman-sennefelder.webp"

const StyledSeparator = styled(SeparatorPrimitive.Root, {
    backgroundColor: "#888",
    margin: "64px 0 32px 0",
    height: 2,
    borderRadius: 8,
})

function TeamManagement(props) {
    const { t, i18n } = useTranslation("common")
    const { scrollY } = useViewportScroll()
    const y1 = useTransform(scrollY, [0, 100], [0, 200])
    const y2 = useTransform(scrollY, [0, 100], [0, 600])
    console.log("contact props", props, t("pages.contact"), scrollY)

    useEffect(() => {
        scrollY.onChange((y) => console.log("y", y))
    }, [])

    const team = [
        {
            id: "emre",
            src: emreBoyacigil,
            name: t("pages.managementTeam.person1.name"),
            department: t("pages.managementTeam.person1.department"),
            p1: t("pages.managementTeam.person1.p1"),
            p2: t("pages.managementTeam.person1.p2"),
            p3: t("pages.managementTeam.person1.p3"),
        },
        {
            id: "eva",
            src: evaSchneider,
            name: t("pages.managementTeam.person2.name"),
            department: t("pages.managementTeam.person2.department"),
            p1: t("pages.managementTeam.person2.p1"),
            p2: t("pages.managementTeam.person2.p2"),
            p3: t("pages.managementTeam.person2.p3"),
        },
        {
            id: "daniel",
            src: danielSkibiorski,
            name: t("pages.managementTeam.person3.name"),
            department: t("pages.managementTeam.person3.department"),
            p1: t("pages.managementTeam.person3.p1"),
            p2: t("pages.managementTeam.person3.p2"),
            p3: t("pages.managementTeam.person3.p3"),
        },
        {
            id: "roman",
            src: romanSennefelder,
            name: t("pages.managementTeam.person5.name"),
            department: t("pages.managementTeam.person5.department"),
            p1: t("pages.managementTeam.person5.p1"),
            p2: t("pages.managementTeam.person5.p2"),
            p3: t("pages.managementTeam.person5.p3"),
        },
        {
            id: "allen",
            src: allenSmead,
            name: t("pages.managementTeam.person4.name"),
            department: t("pages.managementTeam.person4.department"),
            p1: t("pages.managementTeam.person4.p1"),
            p2: t("pages.managementTeam.person4.p2"),
            p3: t("pages.managementTeam.person4.p3"),
        },
        {
            id: "matthias",
            src: matthiasAxenbeck,
            name: t("pages.managementTeam.person6.name"),
            department: t("pages.managementTeam.person6.department"),
            p1: t("pages.managementTeam.person6.p1"),
            p2: t("pages.managementTeam.person6.p2"),
            p3: t("pages.managementTeam.person6.p3"),
        },
    ]
    return (
        <Fragment>
            <Head>
                <title>{t("pages.contact.metaTitle")}</title>
                <meta
                    name="description"
                    content={t("pages.contact.metaDescription")}
                />
            </Head>
            <div className="bg-white pb-64" id="contact-page">
                <motion.section className="pt-12 overflow-hidden bg-white sm:pt-16 lg:pt-20 xl:pt-24 z-0 relative">
                    <div className="px-4 mx-auto sm:px-6 lg:px-8 max-w-7xl">
                        <div className="max-w-lg mx-auto text-center">
                            <h2 className="text-3xl font-semibold tracking-tight text-gray-900 sm:text-4xl lg:text-5xl">
                                {t("pages.managementTeam.title")}
                            </h2>
                            <p className="mt-4 text-base font-normal leading-7 text-gray-600 lg:text-lg lg:mt-6 lg:leading-8">
                                MEET OUR TEAM
                            </p>
                        </div>
                    </div>

                    <div className="relative flex justify-center max-w-5xl mx-auto -mb-8  overflow-y-visible xl:-mb-16  snap-x lg:pt-2 mt-8 rounded-lg">
                        <Image
                            className="object-cover w-full h-auto rounded-md"
                            src={teamImage}
                            alt=""
                        />
                    </div>
                </motion.section>

                <motion.section className="pt-12 overflow-hidden bg-white sm:pt-16 lg:pt-20 xl:pt-24 w-full h-auto z-10 relative">
                    <div className="w-full mt-12 space-y-12 sm:mt-16 lg:mt-20 flex flex-col items-center">
                        {team.map((person) => (
                            <div
                                className="flex flex-wrap mx-auto w-full flex-col items-center"
                                key={person.id}
                            >
                                <div className="w-full lg:w-3/4  xl:max-w-6xl px-4 mb-10 lg:mb-0">
                                    <div className="pt-6 px-8 pb-12 bg-gray-100 rounded-xl">
                                        <div className="md:text-right mb-8">
                                            <span className="p-2 text-sm bg-white person-department rounded-full uppercase font-bold text-gray-300">
                                                {person.department}
                                            </span>
                                        </div>
                                        <div className="flex flex-wrap md:flex-nowrap">
                                            <div className="w-full md:w-1/4 rounded-md md:mr-12">
                                                <Image
                                                    className="relative w-full min-h-[100%]  rounded-md -bottom-4 "
                                                    src={person.src}
                                                    alt={person.name}
                                                />
                                            </div>
                                            <div className="w-full md:w-3/4">
                                                <h4 className="mb-6 text-3xl font-heading text-gray-600 font-semibold title-font tracking-tighter">
                                                    {person.name}
                                                </h4>
                                                <p className="text-xl person-bio mt-4 text-gray-400 title-font tracking-tighter">
                                                    {person.p1}
                                                </p>
                                                <p className="text-xl person-bio mt-4 text-gray-400 title-font tracking-tighter">
                                                    {person.p2}
                                                </p>
                                                <p className="text-xl person-bio mt-4 text-gray-400 title-font tracking-tighter">
                                                    {person.p3}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </motion.section>
            </div>
        </Fragment>
    )
}
export async function getStaticProps({ locale }) {
    return {
        props: {
            ...(await serverSideTranslations(
                locale,
                ["common"],
                nextI18NextConfig
            )),
        },
    }
}

export default TeamManagement
