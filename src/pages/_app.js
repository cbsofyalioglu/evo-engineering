import { Fragment } from 'react';
import '@splidejs/react-splide/css/sea-green';
import '../styles/swiper-bundle.css';
import '../../public/stylesheets/global.css';
import Head from 'next/head';
import { ScrollToTop } from '../components/scroll';
//import ContextLayout from "../layout/context"
import { Layout } from '../layout/layout';
import { appWithTranslation } from 'next-i18next';
import nextI18NextConfig from '../../next-i18next.config';
import { Theme } from '@radix-ui/themes';
import '@radix-ui/themes/styles.css';

function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Theme>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </Theme>
      <ScrollToTop />
    </Fragment>
  );
}

export default appWithTranslation(MyApp, nextI18NextConfig);
