import React, { useRef, useEffect, useState, Fragment } from "react"
import Head from "next/head"
import Image from "next/image"
import { serverSideTranslations } from "next-i18next/serverSideTranslations"
import nextI18NextConfig from "../../next-i18next.config"
import { useTranslation } from "next-i18next"
import { styled } from "../styles/stitches.config"
import * as SeparatorPrimitive from "@radix-ui/react-separator"
import { FadingHero } from "../components/fading-hero"
import Link from "next/link"
import { PageLayout } from "../layout/page-layout"
import { VIDEO_SOURCE_FORMATS } from "../settings/constant"

const StyledSeparator = styled(SeparatorPrimitive.Root, {
    backgroundColor: "#888",
    margin: "64px 0 32px 0",
    height: 2,
    borderRadius: 8,
})

const CareerPage = (props) => {
    const { t, i18n } = useTranslation("common")
    const vref = useRef(null)
    const [sizes, setSizes] = useState({
        width: 1920,
        height: 1080
    })
    useEffect(() => {
        if (vref && vref.current) {
            const currentSize = {
                width: vref.current.clientWidth,
                height: vref.current.clientHeight
            }
            if (
                Math.floor(sizes.width) !==
                Math.floor(vref.current.clientWidth)
            ) {
                setSizes(currentSize)
            }
        }
    }, [vref])
    const sections = [
        {
            "title": t("pages.careers.item1.title"),
            "image": t("pages.careers.item1.image"),
            "href": t("pages.careers.item1.href")
        },
        {
            "title": t("pages.careers.item2.title"),
            "image": t("pages.careers.item2.image"),
            "href": t("pages.careers.item2.href")
        },
        {
            "title": t("pages.careers.item3.title"),
            "image": t("pages.careers.item3.image"),
            "href": t("pages.careers.item3.href")
        },
    ]

    return (
        <Fragment>
            <Head>
                <title>{t("pages.careers.metaTitle")}</title>
            </Head>
            <div className="bg-white">
                {/* <FadingHero
                    src={"/img/teams-out.webp"}
                    title={t("pages.about.title")}
                    description={t("pages.about.mission.description")}
                /> */}
                <div className="w-full h-auto flex flex-col items-stretch">
                    <div className="w-full relative min-h-[30vh] sm:min-h-[50vh]">
                        {/* <img
                            src={"/img/teams-out.webp"}
                            className="rounded-lg z-10 absolute inset-0 w-full h-full object-cover"
                            alt="EVO-E Vision"
                        /> */}
                        <video
                            ref={vref}
                            className="relative mx-auto mb-4 sm:mb-12 z-0"
                            width={sizes.width}
                            height={sizes.height}
                            autoPlay
                            muted
                            loop
                            preload="auto"
                            playsInline
                        >
                            {VIDEO_SOURCE_FORMATS.map((format) => (
                                <source
                                    key={format}
                                    src={`/video/career.${format.replace("video/", "")}`} type={format}
                                />
                            ))}
                        </video >
                    </div>

                    {/* CAREERS SECTIONS  */}
                    <PageLayout>
                        <div className="flex flex-col">


                            {/* TITLE  */}
                            <div className="text-center flex flex-col items-center sm:items-start">
                                <h1 className="text-5xl sm:text-7xl font-extrabold tracking-tight text-gray-900 sm:text-7xl">
                                    {t("pages.careers.title1")}
                                </h1>
                                <h1 style={{ color: "#FC7F33" }} className="text-5xl sm:text-7xl font-extrabold tracking-tight sm:text-7xl">{t("pages.careers.title2")}</h1>
                            </div>

                            {/* SECTIONS  */}
                            <CareersSectionGrid sections={sections} t={t} />
                        </div>


                    </PageLayout>
                </div>
            </div>
        </Fragment>
    )
}

export const CareersSection = ({
    title,
    subtitle,
    description,
    features,
    linkTitle
}) => (
    <div className="relative bg-gray-50 py-16 sm:py-24 lg:py-32">
        <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:px-8 lg:max-w-7xl">
            <div className="mt-12">
                <div className="grid grid-cols-1 gap-16 sm:grid-cols-2">
                    {features?.map((feature, i) => (
                        <div
                            key={"feature-expertise" + i}
                            className="pt-6 shadow-xs"
                        >
                            <div className="flow-root bg-gray-100 rounded-lg overflow-hidden h-60">
                                <div className="w-full h-60 overflow-hidden relative group">
                                    <img
                                        src={feature.icon}
                                        className="object-cover lg-rounded lg:h-full lg:w-full group-hover:scale-105 ease-in-out duration-700 transform-gpu"
                                    />
                                    <div className="absolute top-0 bottom-0 left-0 right-0 bg-gradient-to-t from-[rgba(0,0,0,0.4)] to-transparent"></div>
                                    <h4 className="mt-8 mb-6 absolute bottom-0 group-hover:bottom-6 ease-out duration-700 w-full uppercase text-2xl font-black text-gray-50 tracking-tight ">
                                        {feature.title}
                                    </h4>
                                    {/*<Link href={"#"}>
                                        <a
                                            className="absolute top-0 bottom-0 left-0 right-0"
                                            title={linkTitle}
                                        ></a>
                                    </Link>*/}
                                    {/*<p className="mt-5 text-base text-gray-500">
                                        {feature.description}
                                    </p>*/}
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    </div>
)

const CareersSectionGrid = (props) => (
    <div className="relative w-full">
        <div className="text-center w-full">
            <div className="mt-4 w-full">
                <div className="grid grid-cols-1 gap-4 lg:grid-cols-3 !w-full">
                    {props.sections?.map((section, i) => (
                        <div
                            key={"feature-expertise" + i}
                            className="pt-6 shadow-xs w-full"
                        >
                            <div className="flow-root bg-gray-100 rounded-lg overflow-hidden h-60">
                                <div className="w-full h-60 overflow-hidden relative group">
                                    <img
                                        src={section.image}
                                        className="object-fill lg-rounded lg:h-full lg:w-full group-hover:scale-105 ease-in-out duration-700 transform-gpu"
                                    />
                                    <div className="absolute top-0 bottom-0 left-0 right-0 bg-gradient-to-t from-[rgba(0,0,0,0.4)] to-transparent"></div>
                                    <h4 className="mt-8 mb-6 absolute bottom-0 group-hover:bottom-6 ease-out duration-700 w-full uppercase text-2xl font-black text-gray-50 tracking-tight ">
                                        {props.t(section.title)}
                                    </h4>
                                    <Link href={section.href}>
                                        <a
                                            className="absolute top-0 bottom-0 left-0 right-0"
                                            title={props.t(section.title)}
                                        ></a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    </div>

)

export async function getStaticProps({ locale }) {
    return {
        props: {
            ...(await serverSideTranslations(
                locale,
                ["common"],
                nextI18NextConfig
            )),
        },
    }
}

export default CareerPage
